Установка самого DDOTS
======================

ВАЖНО: Пожалуйста, подготовьте все зависимости как это описано в 
[README](README_ru.md) (и соответствующих `INSTALL_ON_*` инструкциях) перед
продолжением.


Скачиваем DDOTS
---------------

```bash
$ git clone --recurse-submodules https://gitlab.com/dots.org.ua/ddots.git
```


Инициализируем DDOTS
--------------------

На этом этапе скачивается порядка 3ГБ образов (компиляторы), которые
распаковываются в 5ГБ пространства занимаемого на диске:

```bash
$ cd ddots
$ make install
```


Настраиваем DDOTS
-----------------

Копируем шаблон `testing_system/local_settings.py.template` в `testing_system/local_settings.py`
и редактируем его (ссылка на DOTS, логин/пароль DOTS, time factor и другое).
