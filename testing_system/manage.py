#!/usr/bin/env python
# encoding: utf-8
"""
This is the entry point script to DDOTS.
"""

from pathlib import Path
import signal
import sys


def terminate(signum, frame):
    # pylint: disable=unused-argument
    sys.stderr.write("Terminating...\n")
    from worker import current_app
    current_app.stop()


def main():
    """
    This is the entry point to DDOTS.
    """
    command = sys.argv[1]

    if command == 'start':
        from worker import create_app

        app = create_app()
        signal.signal(signal.SIGTERM, terminate)
        app.run()

    elif command == 'test_solution':
        import logging
        import os

        from worker import test_solution

        problem_root = Path(os.environ.get('PROBLEM_ROOT', '/tmp/ddots-test_solution/problem'))
        if not (problem_root / 'Problem.xml').exists():
            logging.error(
                    "Problem.xml doesn't exist in `%s`. Please, fix the PROBLEM_ROOT.",
                    problem_root
                )
            return

        testing_mode = os.environ.get('TESTING_MODE', 'full')

        if 'PROGRAMMING_LANGUAGE' not in os.environ:
            logging.error(
                    "PROGRAMMING_LANGUAGE environment variable is required to test a solution."
                )
            return
        programming_language = os.environ['PROGRAMMING_LANGUAGE']

        solution_path = Path(os.environ.get('SOLUTION', '/tmp/ddots-test_solution/solution.source'))
        if not solution_path.exists():
            logging.error("'%s' does not exist.", solution_path)

        test_report = test_solution(
                programming_language,
                solution_path,
                problem_root,
                testing_mode
            )
        print(test_report)

main()
