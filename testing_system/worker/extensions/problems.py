# encoding: utf-8
"""
DOTS Problems module
====================
"""
import logging
import os
import operator
from pathlib import Path
import shutil
import tarfile

import lockfile
import requests
import xmltodict

from ..exceptions import TestingSystemError
from ..utils import download_file, force_async


log = logging.getLogger(__name__)  # pylint: disable=invalid-name


def download_problem_package(url, local_path, lock_timeout=10, http_timeout=None, **kwargs):
    """
    Download and extract problem files (Problem.xml, tests, and a checker).

    Args:
        url (str): URL to the remote problem .tar.gz archive.
        local_path (str): the destination path.
        lock_timeout (float): the problem files can be requested concurrently
            and we might just need to wait until the other process finishes the
            download.
        http_timeout (float): timeout for HTTP requests.

    Returns:
        is_new (bool): a flag is set if the problem was updated.
    """
    log.debug("Fetching a problem package into '%s'...", local_path)
    lock = lockfile.LockFile(local_path)
    try:
        lock.acquire(timeout=lock_timeout)
    except lockfile.LockTimeout:
        log.info(
            "Folder '%s' is locked. Probably, another instance is still downloading it.",
            local_path
        )
        raise
    try:
        problem_package_path = local_path.with_suffix('.tar.gz')
        is_new = download_file(
            url,
            problem_package_path,
            lock_timeout=lock_timeout,
            http_timeout=http_timeout,
            force_etag=local_path.exists(),
            **kwargs
        )
        if is_new:
            try:
                shutil.rmtree(local_path)
            except FileNotFoundError:
                pass
            log.info("Unpacking a problem to '%s'...", local_path)
            # Problem archives should always contain only one folder with everything else inside,
            # but just to make sure that wrong archive won't break our problems_db we make this
            # 'complex' extraction.
            with tarfile.open(problem_package_path) as problem_package:
                local_name_prefix = '%s/' % local_path.name
                problem_package.extractall(
                    path=local_path.parent,
                    members=(
                        tarinfo for tarinfo in problem_package \
                            if tarinfo.name.startswith(local_name_prefix)
                    )
                )
            problem_package_path.unlink()
        log.debug("The problem is available now in '%s' folder", local_path)
        return is_new
    finally:
        lock.release()


class ProblemsManager(object):
    """
    Local problems DB manager.
    """
    def __init__(self, app=None):
        if app is not None:
            self.init_app(app)

    def init_app(self, app):
        self._db_root = app.config.PROBLEMS_DB_ROOT
        self._db_max_cached_problems = app.config.PROBLEMS_DB_MAX_CACHED_PROBLEMS
        self._problem_download_timeout = app.config.DOTS_PROBLEM_DOWNLOAD_TIMEOUT
        self._db_root.mkdir(parents=True, exist_ok=True)

    def get_problem_path(self, problem_id):
        """
        Args:
            problem_id (str): a unique ID of a problem.

        Returns:
            problem_path (str): a problem path in the problems DB.
        """
        return self._db_root / problem_id

    @force_async
    def prepare_problem(self, problem_id):
        """
        Ensure that the problem files exist and ready to be used.

        Args:
            problem_id (str): a unique ID of a problem.

        Returns:
            problem_path (str): a problem path in the problems DB.
        """
        from . import dots_api

        problem_path = self.get_problem_path(problem_id)

        try:
            is_new = download_problem_package(
                '/tests/%s' % problem_id,
                problem_path,
                lock_timeout=self._problem_download_timeout,
                http_timeout=self._problem_download_timeout,
                session_get=dots_api._post_blocking
            )
        except lockfile.LockTimeout:
            raise TestingSystemError(
                    "problem #%s folder has been locked for too long" % problem_id
                )
        except requests.HTTPError:
            raise TestingSystemError("problem #%s failed to download" % problem_id)

        if is_new:
            self.trim_problems_db_cache(exclude={problem_path.name})
        return problem_path

    def trim_problems_db_cache(self, exclude=None):
        """
        Trim the number of locally cached problems based on the
        ``PROBLEMS_DB_MAX_CACHED_PROBLEMS`` setting.
        """
        if exclude is None:
            exclude = tuple()
        log.debug("Checking whether the problems cache exceeds the storage limit...")
        problem_paths = [
                problem_path for problem_path in self._db_root.iterdir() \
                    if problem_path.is_dir() and problem_path.name not in exclude
            ]
        cache_overflow = len(problem_paths) - self._db_max_cached_problems
        if cache_overflow <= 0:
            return

        problem_paths_with_weight = []
        for problem_path in problem_paths:
            try:
                weight = problem_path.with_suffix('.tar.gz.etag').stat().st_atime
            except IOError:
                weight = 0
            problem_paths_with_weight.append((problem_path, weight))
        problem_paths_with_weight.sort(key=operator.itemgetter(1))
        for problem_path, _ in problem_paths_with_weight[:cache_overflow]:
            try:
                shutil.rmtree(problem_path)
            except IOError:
                continue
            log.info("Problem #%s has been pushed out of cache", problem_path.name)


class ProblemsDockerManagerMixin(object):
    """
    Helper DockerManager Mixin helping to prepare current problem files
    available to the Docker containers.
    """

    PROBLEM_XML_NAME = 'Problem.xml'

    CURRENT_PROBLEM_DIRNAME = 'problem'
    CURRENT_PROBLEM_ROOT_CHMOD = 0o770

    def get_problem_info(self, problem_path):
        """
        Args:
            problem_path (str): a path to a problem folder which contains
                ``Problem.xml``.

        Returns:
            problem_info (dict): information extracted from the
            ``Problem.xml``.
        """
        # Parse Problem.xml to get information about tests
        return xmltodict.parse((problem_path / self.PROBLEM_XML_NAME).read_text())['Problem']

    def _prepare_environment(self, problem_path, problem_info, **kwargs):
        pass

    def prepare_environment(self, problem_path, **kwargs):
        """
        Prepare an environment (folders, files, permissions, etc) before a
        container execution.
        """
        problem_info = self.get_problem_info(problem_path)

        # Prepare problem root
        current_problem_root = (
                self._shared_volumes['data']['mount_point'] /
                self.CURRENT_PROBLEM_DIRNAME
            )
        try:
            shutil.rmtree(current_problem_root)
        except FileNotFoundError:
            pass
        shutil.copytree(problem_path, current_problem_root)

        # chmod files in `current_problem_root` recursively
        current_problem_root.chmod(self.CURRENT_PROBLEM_ROOT_CHMOD)
        executables = {problem_info.get(x) for x in ('@CheckerExe', '@PatcherExe', '@InteractorExe', '@GeneratorExe')}
        for root, dirs, files in os.walk(current_problem_root):
            root = Path(root)
            for _dir in dirs:
                (root / _dir).chmod(self.CURRENT_PROBLEM_ROOT_CHMOD)
            for _file in files:
                (root / _file).chmod(
                    self.CURRENT_PROBLEM_ROOT_CHMOD & (0o777 if _file in executables else 0o666)
                )

        self._prepare_environment(problem_path, problem_info, **kwargs)
