# encoding: utf-8
"""
Compiled-code Runners module
============================
"""
import logging
from pathlib import Path
import re
import shutil

from ..exceptions import TestingSystemError
from .docker import DockerManager
from .compilers import CompilersManager
from .problems import ProblemsDockerManagerMixin


log = logging.getLogger(__name__)  # pylint: disable=invalid-name


class RunnersManager(ProblemsDockerManagerMixin, DockerManager):
    """
    Docker-based Compiled-code Runners Manager
    """

    DOCKER_IMAGE_PREFIX = 'ddots-runner-'

    DEFAULT_CONTAINER_TIMEOUT = 200

    HOST_CGROUP_SYS_FS_ROOT = Path('/sys/fs/cgroup')
    CONTAINER_CGROUP_SYS_FS_ROOT = Path('/sys/fs/cgroup')

    SOLUTION_FILENAME = CompilersManager.SOLUTION_EXECUTABLE_FILENAME
    SOLUTION_FILE_CHMOD = 0o775

    RS_OK = 'OK'
    RS_WRONG_ANSWER = 'WA'
    RS_TIME_LIMIT_EXCEEDED = 'TL'
    RS_MEMORY_LIMIT_EXCEEDED = 'ML'
    RS_PRESENTATION_ERROR = 'PE'
    RS_RUNTIME_ERROR = 'RE'
    RS_UNKNOWN_ERROR = 'UE'
    RS_FORBIDDEN_FUNCTION = 'FF'
    RS_NO_CHECKER = 'NC'
    RS_CHECKER_CRASH = 'CC'
    RS_CHECKER_TIMEOUT = 'CT'

    REPORT_STATUSES = (
            RS_OK,
            RS_WRONG_ANSWER,
            RS_TIME_LIMIT_EXCEEDED,
            RS_MEMORY_LIMIT_EXCEEDED,
            RS_PRESENTATION_ERROR,
            RS_RUNTIME_ERROR,

            RS_UNKNOWN_ERROR,

            # Internal Errors
            RS_FORBIDDEN_FUNCTION,
            RS_NO_CHECKER,
            RS_CHECKER_CRASH,
            RS_CHECKER_TIMEOUT,
        )

    REPORT_STATUSES_INTERNAL = REPORT_STATUSES[REPORT_STATUSES.index(RS_UNKNOWN_ERROR):]

    REPORT_LINE_REGEX = re.compile(
            (
                r'^(?P<test_index>\d+) '
                r'(?P<status>%(supported_statuses)s) '
                r'(?P<points>\d+(?:\.\d+)?) '
                r'(?P<execution_time>\d+) '
                r'(?P<memory_peak>\d+)$'
            )
            % {
                    'supported_statuses': '|'.join(REPORT_STATUSES),
                }
        )

    REPORT_SIZE_LIMIT = 8000

    TM_FULL = 'full'
    TM_FIRST_FAIL = 'first-fail'
    TM_ONE = 'one'

    TESTING_MODES = {TM_FULL, TM_FIRST_FAIL, TM_ONE}

    def __init__(self, *args, **kwargs):
        super(RunnersManager, self).__init__(*args, **kwargs)
        self._container_timeout = None

    def init_app(self, app):
        super(RunnersManager, self).init_app(app)
        self._base_container_name = app.config.BASE_CONTAINER_NAME
        self._solution_filepath = \
            self._shared_volumes['data']['mount_point'] / self.SOLUTION_FILENAME
        self._problem_root = \
            self._shared_volumes['data']['mount_point'] / self.CURRENT_PROBLEM_DIRNAME
        self._cpu_id = app.config.CPU_ID
        self._runner_extra_options = app.config.RUNNER_EXTRA_OPTIONS
        self._runner_stack_hard_limit = app.config.RUNNER_STACK_HARD_LIMIT
        self._runner_time_limit_factor = app.config.RUNNER_TIME_LIMIT_FACTOR
        if self._runner_time_limit_factor is None:
            self._docker_client.images.pull(app.config.RUNNER_TIME_LIMIT_FACTOR_STANDARD_IMAGE)
            time_limit_factor = self._docker_client.containers.run(
                    app.config.RUNNER_TIME_LIMIT_FACTOR_STANDARD_IMAGE,
                    stderr=True
                )
            try:
                self._runner_time_limit_factor = float(time_limit_factor.decode('utf-8'))
            except ValueError:
                log.error("One-second Standard returned an unexpected value: %r", time_limit_factor)
                raise
        log.info("Runner Time Limit Factor is configured to %.2f", self._runner_time_limit_factor)
        self._cgmemtime_manager = CGMemTimeManager()
        self._cgmemtime_manager.init_app(app)
        self.init_containers_list(set(app.config.DOTS_LANG_ID_TO_RUNNER.values()))

    def _get_container_name(self, container_suffix):
        return '%s%s-%s' % (self.DOCKER_IMAGE_PREFIX, self._base_container_name, container_suffix)

    @classmethod
    def get_cgroup_base_name(cls, base_container_name):
        return cls.DOCKER_IMAGE_PREFIX + base_container_name

    def _get_container_init_options(self, container_suffix):
        container_init_options = super(RunnersManager, self)\
            ._get_container_init_options(container_suffix)
        container_init_options['user'] = 'root'
        container_init_options['entrypoint'] = '/usr/local/bin/ddots-runner'

        host_config = {
                'ulimits': [
                        {
                            'Name': 'stack',
                            'Soft': self._runner_stack_hard_limit,
                            'Hard': self._runner_stack_hard_limit,
                        },
                    ],
            }
        if not self._runner_extra_options.get(container_suffix, {}).get('ignore_cpu_pinning'):
            host_config['cpuset_cpus'] = str(self._cpu_id)
        container_init_options['host_config'].update(
                self._docker_client.api.create_host_config(**host_config)
            )

        for cgroup_set in self._cgmemtime_manager.CGROUPS:
            cgroup_set_host_path = str(
                    self.HOST_CGROUP_SYS_FS_ROOT /
                    cgroup_set /
                    self.get_cgroup_base_name(self._base_container_name)
                )
            container_init_options['volumes'].append(cgroup_set_host_path)
            extra_host_config = self._docker_client.api.create_host_config(
                    binds={
                            cgroup_set_host_path: {
                                    'bind': str(self.CONTAINER_CGROUP_SYS_FS_ROOT / cgroup_set),
                                    'mode': 'rw',
                                },
                        }
                )
            container_init_options['host_config']['Binds'] += extra_host_config['Binds']

        container_init_options['environment'].update({
            'RUST_LOG': 'ddots_runner={}'.format(logging.getLevelName(log.getEffectiveLevel())),
            'DDOTS_RUNNER_SANDBOX_CGROUP_NAMESPACE': 'ddots-runner',
            'DDOTS_RUNNER_REPORT_OUTPUT_FORMAT': 'legacy',
            'DDOTS_RUNNER_SOLUTION_FILEPATH': str(self._solution_filepath),
            'DDOTS_RUNNER_PROBLEM_ROOT': str(self._problem_root),
            'DDOTS_RUNNER_SANDBOX_ROOT': str(self._shared_volumes['sandbox']['mount_point']),
            'DDOTS_RUNNER_TIME_FACTOR': '%f' % (
                    self._runner_time_limit_factor
                    *
                    self._runner_extra_options\
                        .get(container_suffix, {})\
                        .get('time_limit_multiplier', 1.0)
                ),
        })

        return container_init_options

    def init_containers_list(self, containers_suffix_list):
        log.info("Initializing runner containers...")
        super(RunnersManager, self).init_containers_list(containers_suffix_list)

    def _prepare_environment(
            self,
            problem_path, problem_info,
            solution_executable_path=None,
            testing_mode=None
        ):  # pylint: disable=arguments-differ
        """
        Prepare paths structure for a runner container
        """
        # Propagate testing mode via a `testing_mode` file in `DATA_ROOT/`
        assert testing_mode in self.TESTING_MODES, "expected a supported testing_mode"
        with open(self._problem_root / 'testing_mode', 'w') as testing_mode_flag_file:
            testing_mode_flag_file.write(testing_mode)

        # Copy a solution to a predefined path
        shutil.copyfile(solution_executable_path, self._solution_filepath)
        self._solution_filepath.chmod(self.SOLUTION_FILE_CHMOD)

        # Set the whole running timout based on the number of tests and problem TL
        problem_time_limit = float(problem_info['@TimeLimit'])
        problem_tests_count = int(problem_info['@TestCount'])
        self._container_timeout = max(
                problem_time_limit * self._runner_time_limit_factor * problem_tests_count * 5,
                self.DEFAULT_CONTAINER_TIMEOUT
            )

    def truncate_report(self, report):
        """
        Truncate report if it is too big.
        """
        if len(report) > self.REPORT_SIZE_LIMIT:
            report = report[:self.REPORT_SIZE_LIMIT]
            report = report[:report.rindex('\n')]
            report += '\n(truncated report)\n'
        return report

    def _validate_report(self, problem_path, testing_mode, report):
        """
        Validate a report received from a runner container and add bonus points if necessary.
        """
        problem_info = self.get_problem_info(problem_path)

        sum_of_points = 0.0
        all_tests_succeeded = True

        report_lines = report.split('\n')
        # A report may contain an error details after an empty line after all test-result lines
        try:
            empty_line_index = report_lines.index('')
        except ValueError:
            pass
        else:
            report_lines = report_lines[:empty_line_index]

        for line in report_lines:
            try:
                test_report = self.REPORT_LINE_REGEX.match(line).groupdict()
            except AttributeError:
                raise TestingSystemError(
                    "The testing report is corrupted (line: '%s')\n%s" % (line, report)
                )

            if test_report['status'] == 'OK':
                sum_of_points += float(test_report['points'])
            else:
                all_tests_succeeded = False

        if not report_lines:
            raise TestingSystemError("The testing report is empty.")

        tests_count = int(problem_info['@TestCount'])

        if (
                testing_mode == self.TM_FULL
                or
                (testing_mode == self.TM_FIRST_FAIL and all_tests_succeeded)
            ) and len(report_lines) != tests_count:
            # Handle the case when a report finishes with unrecoverable error
            # i.e. RS_NO_CHECKER, RS_CHECKER_CRASH, RS_FORBIDDEN_FUNCTION, etc.
            test_report = self.REPORT_LINE_REGEX.match(report_lines[-1]).groupdict()
            if test_report['status'] not in self.REPORT_STATUSES_INTERNAL:
                raise TestingSystemError(
                    "The testing report is corrupted. The number of tests "
                    "is not equal to the number of reported lines."
                )

        if all_tests_succeeded \
                and testing_mode in (self.TM_FULL, self.TM_FIRST_FAIL) \
                and '@PointsOnGold' in problem_info:
            # Add bonus points
            report += '%d OK %.2f 0 0\n' % (
                    tests_count + 1,
                    float(problem_info['@PointsOnGold']) - sum_of_points
                )

        return self.truncate_report(report)

    def start(
            self,
            container_suffix,
            problem_path,
            testing_mode,
            solution_executable_path,
            timeout=None, **kwargs
        ):
        # pylint: disable=too-many-arguments
        assert timeout is None, "custom timeout is not supported"

        self.prepare_environment(
                problem_path,
                solution_executable_path=solution_executable_path,
                testing_mode=testing_mode
            )

        # Run tests
        runner_exit_code, (runner_stdout, runner_stderr) = super(RunnersManager, self).start(
                container_suffix,
                timeout=self._container_timeout,
                **kwargs
            )

        if runner_stderr:
            log.info("DDOTS Runner reported the following logs:\n%s", runner_stderr)

        if runner_exit_code != 0:
            raise TestingSystemError("DDOTS Runner exited with non-zero exit code.")

        # stdout must contain DOTS testing report
        return self._validate_report(problem_path, testing_mode, runner_stdout)


class CGMemTimeManager(DockerManager):

    DOCKER_IMAGE_PREFIX = RunnersManager.DOCKER_IMAGE_PREFIX
    HOST_CGROUP_SYS_FS_ROOT = RunnersManager.HOST_CGROUP_SYS_FS_ROOT
    CONTAINER_CGROUP_SYS_FS_ROOT = RunnersManager.CONTAINER_CGROUP_SYS_FS_ROOT

    CGROUPS = ['memory', 'cpuacct', 'pids', 'freezer']

    def init_app(self, app):
        super(CGMemTimeManager, self).init_app(app)
        self._base_container_name = app.config.BASE_CONTAINER_NAME
        self.init_container('binary')
        self.start('binary')
        self.rm_container('binary')

    def _get_container_name(self, container_suffix):
        return '%s%s-%s' % (self.DOCKER_IMAGE_PREFIX, self._base_container_name, container_suffix)

    def _get_container_init_options(self, container_suffix):
        container_init_options = super(CGMemTimeManager, self)._get_container_init_options(
                container_suffix
            )
        cgroup_base_name = RunnersManager.get_cgroup_base_name(self._base_container_name)
        container_init_options.update({
                'user': 'root',
                'entrypoint': '/bin/sh',
                'command': (
                        '-c',
                        ''.join(
                            (
                                "rmdir '%(cgroup_path)s';"
                                "mkdir -p '%(cgroup_path)s';"
                                "chown operator:root '%(cgroup_path)s';"
                                "chmod 700 '%(cgroup_path)s';"
                            ) % {
                                    'cgroup_path': (
                                        self.CONTAINER_CGROUP_SYS_FS_ROOT /
                                        cgroup /
                                        cgroup_base_name /
                                        'ddots-runner'
                                    ),
                                }
                            for cgroup in self.CGROUPS
                        )
                    ),
            })
        host_cgroup_sys_fs_root = str(self.HOST_CGROUP_SYS_FS_ROOT)
        container_init_options['volumes'].append(host_cgroup_sys_fs_root)
        container_init_options['host_config']['Binds'] += self._docker_client.api.create_host_config(
                binds={
                        host_cgroup_sys_fs_root: {
                                'bind': str(self.CONTAINER_CGROUP_SYS_FS_ROOT),
                                'mode': 'rw',
                            },
                    }
            )['Binds']
        return container_init_options
