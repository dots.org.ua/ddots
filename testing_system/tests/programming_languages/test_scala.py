# encoding: utf-8
from pathlib import Path
import pytest

from worker.exceptions import CompilationError


def test_scala_OK(compiler_runner, tmpdir, hello_world_problem):
    OK_solution_path = tmpdir.join('OK_solution.scala')
    OK_solution_path.write(
            'import java.nio.file.{Paths, Files};'
            'import java.nio.charset.StandardCharsets;'
            'object Main extends App {'
            '    Files.write('
            '        Paths.get("output.txt"),'
            '        "Hello World!".getBytes(StandardCharsets.UTF_8)'
            '    )'
            '}'
        )
    testing_report = compiler_runner(
            'scala',
            problem_path=hello_world_problem['path'],
            solution_source_path=Path(OK_solution_path),
            testing_mode='one'
        )
    assert testing_report.split('\n')[0].split(' ')[:3] == ['1', 'OK', '3.8']


def test_scala_compiler_CE(compiler, tmpdir, hello_world_problem):
    CE_solution_path = tmpdir.join('CE_solution.scala')
    CE_solution_path.write('if')
    with pytest.raises(CompilationError) as CE:
        compiler(
                'scala',
                problem_path=hello_world_problem['path'],
                solution_source_path=Path(CE_solution_path)
            )
    assert 'solution.source:1: error: expected class or object definition' in str(CE.value)
