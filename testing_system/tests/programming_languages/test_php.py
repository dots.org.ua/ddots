# encoding: utf-8
from pathlib import Path
import pytest

from worker.exceptions import CompilationError


def test_php_OK(compiler_runner, tmpdir, hello_world_problem):
    OK_solution_path = tmpdir.join('OK_solution.php')
    OK_solution_path.write(
            '<?php $fout = fopen("output.txt", "w"); fwrite($fout, "Hello World!"); ?>'
        )
    testing_report = compiler_runner(
            'php',
            problem_path=hello_world_problem['path'],
            solution_source_path=Path(OK_solution_path),
            testing_mode='one'
        )
    assert testing_report.split('\n')[0].split(' ')[:3] == ['1', 'OK', '3.8']


def test_php_compiler_CE(compiler, tmpdir, hello_world_problem):
    CE_solution_path = tmpdir.join('CE_solution.php')
    CE_solution_path.write('if')
    with pytest.raises(CompilationError) as CE:
        compiler(
                'php',
                problem_path=hello_world_problem['path'],
                solution_source_path=Path(CE_solution_path)
            )
    assert (
            'solution.source: Testing System Compilation Error: There must be a `<?php` tag' \
            in str(CE.value)
        )
