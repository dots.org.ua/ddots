# encoding: utf-8
from pathlib import Path
import pytest

from worker.exceptions import CompilationError


def test_swift_OK(compiler_runner, tmpdir, hello_world_problem):
    OK_solution_path = tmpdir.join('OK_solution.swift')
    OK_solution_path.write(
            'import Foundation\n'
            'FileManager.default.createFile(atPath: "output.txt", contents: "Hello World!".data(using: .utf8))'
        )
    testing_report = compiler_runner(
            'swift',
            runner='swift',
            problem_path=hello_world_problem['path'],
            solution_source_path=Path(OK_solution_path),
            testing_mode='one'
        )
    assert testing_report.split('\n')[0].split(' ')[:3] == ['1', 'OK', '3.8']


def test_swift_compiler_CE(compiler, tmpdir, hello_world_problem):
    CE_solution_path = tmpdir.join('CE_solution.swift')
    CE_solution_path.write('if')
    with pytest.raises(CompilationError) as CE:
        compiler(
                'swift',
                problem_path=hello_world_problem['path'],
                solution_source_path=Path(CE_solution_path)
            )
    assert 'solution.source' in str(CE.value)
    assert 'error: expected expression' in str(CE.value)
