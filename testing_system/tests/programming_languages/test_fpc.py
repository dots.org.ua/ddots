# encoding: utf-8
from pathlib import Path
import pytest

from worker.exceptions import CompilationError


@pytest.mark.parametrize('compiler_image', ('fpc', 'fpc-delphi'))
def test_fpc_OK(compiler_image, compiler_runner, tmpdir, hello_world_problem):
    OK_solution_path = tmpdir.join('OK_solution.pas')
    OK_solution_path.write(
            'uses crt, math, sysutils, strutils, contnrs, dateutils, graph, graphabc, pd_console;'
            'begin'
            '  assign(output, \'output.txt\');'
            '  rewrite(output);'
            '  writeln(\'Hello World!\');'
            'end.'
        )
    testing_report = compiler_runner(
            compiler_image,
            runner='binary',
            problem_path=hello_world_problem['path'],
            solution_source_path=Path(OK_solution_path),
            testing_mode='one'
        )
    assert testing_report.split('\n')[0].split(' ')[:3] == ['1', 'OK', '3.8']


@pytest.mark.parametrize('compiler_image', ('fpc', 'fpc-delphi'))
def test_fpc_math_OK(compiler_image, compiler_runner, tmpdir, hello_world_problem):
    OK_solution_path = tmpdir.join('OK_solution.pas')
    OK_solution_path.write(
            'var test: real;'
            'begin'
            '  assign(output, \'output.txt\');'
            '  rewrite(output);'
            '  test := sqrt(2);'
            '  writeln(\'Hello World!\');'
            'end.'
        )
    testing_report = compiler_runner(
            compiler_image,
            runner='binary',
            problem_path=hello_world_problem['path'],
            solution_source_path=Path(OK_solution_path),
            testing_mode='one'
        )
    assert testing_report.split('\n')[0].split(' ')[:3] == ['1', 'OK', '3.8']


def test_fpc_integer_is_16bit(compiler_runner, tmpdir, hello_world_problem):
    OK_solution_path = tmpdir.join('OK_solution.pas')
    OK_solution_path.write(
            'var test: integer;'
            'begin'
            '  assign(output, \'output.txt\');'
            '  rewrite(output);'
            '  test := 32767;'
            '  inc(test);'
            '  if test < 0 then'
            '    writeln(\'Hello World!\');'
            'end.'
        )
    testing_report = compiler_runner(
            'fpc',
            runner='binary',
            problem_path=hello_world_problem['path'],
            solution_source_path=Path(OK_solution_path),
            testing_mode='one'
        )
    assert testing_report.split('\n')[0].split(' ')[:3] == ['1', 'OK', '3.8']


@pytest.mark.parametrize('compiler_image', ('fpc', 'fpc-delphi'))
def test_fpc_compiler_CE(compiler_image, compiler, tmpdir, hello_world_problem):
    CE_solution_path = tmpdir.join('CE_solution.pas')
    CE_solution_path.write('if')
    with pytest.raises(CompilationError) as CE:
        compiler(
                compiler_image,
                problem_path=hello_world_problem['path'],
                solution_source_path=Path(CE_solution_path)
            )
    assert 'Fatal: Syntax error, "BEGIN" expected but "IF" found' in str(CE.value)


@pytest.mark.parametrize('compiler_image', ('fpc', 'fpc-delphi'))
def test_fpc_compiler_CE_on_real_to_integer_assignment(
        compiler_image,
        compiler,
        tmpdir,
        hello_world_problem
    ):
    CE_solution_path = tmpdir.join('CE_solution.pas')
    CE_solution_path.write('var x: integer; begin x := 1.5; end.')
    with pytest.raises(CompilationError) as CE:
        compiler(
                compiler_image,
                problem_path=hello_world_problem['path'],
                solution_source_path=Path(CE_solution_path)
            )
    assert 'solution.source(1,28) Error: Incompatible types' in str(CE.value)
