# encoding: utf-8
from pathlib import Path
import pytest

from worker.exceptions import CompilationError


def test_ocaml_OK(compiler_runner, tmpdir, hello_world_problem):
    OK_solution_path = tmpdir.join('OK_solution.ml')
    OK_solution_path.write(
            'let () =\n'
            '  let oc = open_out "output.txt" in\n'
            '  Printf.fprintf oc "Hello World!";\n'
            '  close_out oc;'
        )
    testing_report = compiler_runner(
            'ocaml',
            runner='binary',
            problem_path=hello_world_problem['path'],
            solution_source_path=Path(OK_solution_path),
            testing_mode='one'
        )
    assert testing_report.split('\n')[0].split(' ')[:3] == ['1', 'OK', '3.8']


def test_ocaml_compiler_CE(compiler, tmpdir, hello_world_problem):
    CE_solution_path = tmpdir.join('CE_solution.ml')
    CE_solution_path.write('if')
    with pytest.raises(CompilationError) as CE:
        compiler(
                'ocaml',
                problem_path=hello_world_problem['path'],
                solution_source_path=Path(CE_solution_path)
            )
    assert 'solution.source' in str(CE.value)
    assert 'Error: Syntax error' in str(CE.value)
