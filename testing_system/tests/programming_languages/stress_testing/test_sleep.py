# encoding: utf-8
from pathlib import Path


def test_forbidden_function_sleep(compiler_runner, tmpdir, hello_world_problem):
    solution_path = tmpdir.join('sleepy.py')
    solution_path.write(
            'import time\n'
            'time.sleep(100)\n'
        )
    testing_report = compiler_runner(
            'python3',
            problem_path=hello_world_problem['path'],
            solution_source_path=Path(solution_path),
            testing_mode='full'
        )
    splitted_testing_report = testing_report.split('\n')
    assert len(splitted_testing_report) == 4
    assert splitted_testing_report[0].split(' ')[:3] == ['1', 'FF', '0']
    assert splitted_testing_report[1] == ''
    assert 'sleep' in splitted_testing_report[2]
