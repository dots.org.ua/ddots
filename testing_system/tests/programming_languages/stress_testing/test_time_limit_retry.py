# encoding: utf-8
from pathlib import Path


def test_time_limit_retry(compiler_runner, tmpdir, hello_world_problem):
    solution_path = tmpdir.join('time_limit_retry.sh')
    solution_path.write(
            'if [ "$(cat input.txt)" = "" ]; then\n'
            '    echo "Hello World!" > output.txt\n'
            '    exit 1\n'
            'fi\n'
            'while true; do :; done'
        )
    testing_report = compiler_runner(
            'bash',
            problem_path=hello_world_problem['path'],
            solution_source_path=Path(solution_path),
            testing_mode='full'
        )
    splitted_testing_report = testing_report.split('\n')
    assert len(splitted_testing_report) == 3
    assert splitted_testing_report[0].split(' ')[:3] == ['1', 'OK', '3.8']
    assert splitted_testing_report[1].split(' ')[:3] == ['2', 'TL', '0']
