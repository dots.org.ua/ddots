# encoding: utf-8
from pathlib import Path
import pytest

from worker.exceptions import CompilationError


def test_kotlin_OK(compiler_runner, tmpdir, hello_world_problem):
    OK_solution_path = tmpdir.join('OK_solution.kt')
    OK_solution_path.write(
            'import java.io.File\n'
            'fun main(args : Array<String>) {'
            '    File("output.txt").printWriter().use { out ->'
            '        out.println("Hello World!")'
            '    }'
            '}'
        )
    testing_report = compiler_runner(
            'kotlin',
            problem_path=hello_world_problem['path'],
            solution_source_path=Path(OK_solution_path),
            testing_mode='one'
        )
    assert testing_report.split('\n')[0].split(' ')[:3] == ['1', 'OK', '3.8']

def test_kotlin_compiler_CE(compiler, tmpdir, hello_world_problem):
    CE_solution_path = tmpdir.join('CE_solution.sh')
    CE_solution_path.write('if')
    with pytest.raises(CompilationError) as CE:
        compiler(
                'kotlin',
                problem_path=hello_world_problem['path'],
                solution_source_path=Path(CE_solution_path)
            )
    assert (
            'solution.source: Testing System Compilation Error: There must be a function named '
            '`main`' in str(CE.value)
        )
