# encoding: utf-8
from pathlib import Path
import pytest

from worker.exceptions import CompilationError


def test_nim_OK(compiler_runner, tmpdir, hello_world_problem):
    OK_solution_path = tmpdir.join('OK_solution.nim')
    OK_solution_path.write('writeFile("output.txt", "Hello World!")')
    testing_report = compiler_runner(
            'nim',
            runner='binary',
            problem_path=hello_world_problem['path'],
            solution_source_path=Path(OK_solution_path),
            testing_mode='one'
        )
    assert testing_report.split('\n')[0].split(' ')[:3] == ['1', 'OK', '3.8']

def test_nim_compiler_CE(compiler, tmpdir, hello_world_problem):
    CE_solution_path = tmpdir.join('CE_solution.nim')
    CE_solution_path.write('if')
    with pytest.raises(CompilationError) as CE:
        compiler(
                'nim',
                problem_path=hello_world_problem['path'],
                solution_source_path=Path(CE_solution_path)
            )
    assert 'solution.source(1, 3) Error: invalid indentation' in str(CE.value)
