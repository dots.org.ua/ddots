# encoding: utf-8
from pathlib import Path
import pytest

from worker.exceptions import CompilationError


@pytest.mark.parametrize('compiler_image', ('gxx11', 'gxx14', 'gxx17'))
def test_gxx11_compiler_OK(compiler_image, compiler_runner, tmpdir, hello_world_problem):
    OK_solution_path = tmpdir.join('OK_solution.c')
    OK_solution_path.write(
            '#include "fstream"\n'
            'int main() {'
            '  auto x = 1;'
            '  std::ofstream fout("output.txt");'
            '  fout << "Hello World!";'
            '  return 0;'
            '}'
        )
    testing_report = compiler_runner(
            compiler_image,
            runner='binary',
            problem_path=hello_world_problem['path'],
            solution_source_path=Path(OK_solution_path),
            testing_mode='one'
        )
    assert testing_report.split('\n')[0].split(' ')[:3] == ['1', 'OK', '3.8']
