# encoding: utf-8
from pathlib import Path
import pytest

from worker.exceptions import CompilationError


def test_python2_OK(compiler_runner, tmpdir, hello_world_problem):
    OK_solution_path = tmpdir.join('OK_solution.py')
    OK_solution_path.write('with open("output.txt", "w") as fout: fout.write("Hello World!")')
    testing_report = compiler_runner(
            'python2',
            problem_path=hello_world_problem['path'],
            solution_source_path=Path(OK_solution_path),
            testing_mode='one'
        )
    assert testing_report.split('\n')[0].split(' ')[:3] == ['1', 'OK', '3.8']


def test_python2_compiler_CE(compiler, tmpdir, hello_world_problem):
    CE_solution_path = tmpdir.join('CE_solution.py')
    CE_solution_path.write('if')
    with pytest.raises(CompilationError) as CE:
        compiler(
                'python2',
                problem_path=hello_world_problem['path'],
                solution_source_path=Path(CE_solution_path)
            )
    assert 'solution.source' in str(CE.value)
    assert 'SyntaxError: invalid syntax' in str(CE.value)
