# encoding: utf-8
from pathlib import Path
import pytest

from worker.exceptions import CompilationError


def test_haskell_OK(compiler_runner, tmpdir, hello_world_problem):
    OK_solution_path = tmpdir.join('OK_solution.hs')
    OK_solution_path.write(
            'import System.IO\n'
            'main = do\n'
            '    withFile "output.txt" WriteMode $ \\h -> hPutStrLn h "Hello World!"'
        )
    testing_report = compiler_runner(
            'haskell',
            runner='binary',
            problem_path=hello_world_problem['path'],
            solution_source_path=Path(OK_solution_path),
            testing_mode='one'
        )
    assert testing_report.split('\n')[0].split(' ')[:3] == ['1', 'OK', '3.8']


def test_haskell_compiler_CE(compiler, tmpdir, hello_world_problem):
    CE_solution_path = tmpdir.join('CE_solution.sh')
    CE_solution_path.write('if')
    with pytest.raises(CompilationError) as CE:
        compiler(
                'haskell',
                problem_path=hello_world_problem['path'],
                solution_source_path=Path(CE_solution_path)
            )
    assert 'solution.source' in str(CE.value)
    assert 'parse error (possibly incorrect indentation or mismatched brackets)' in str(CE.value)
