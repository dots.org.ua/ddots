# encoding: utf-8
from pathlib import Path
import pytest

from worker.exceptions import CompilationError


def test_javascript_OK(compiler_runner, tmpdir, hello_world_problem):
    OK_solution_path = tmpdir.join('OK_solution.js')
    OK_solution_path.write('require("fs").writeFileSync("output.txt", "Hello World!")')
    testing_report = compiler_runner(
            'javascript',
            problem_path=hello_world_problem['path'],
            solution_source_path=Path(OK_solution_path),
            testing_mode='one'
        )
    assert testing_report.split('\n')[0].split(' ')[:3] == ['1', 'OK', '3.8']


def test_javascript_compiler_CE(compiler, tmpdir, hello_world_problem):
    CE_solution_path = tmpdir.join('CE_solution.js')
    CE_solution_path.write('if q')
    with pytest.raises(CompilationError) as CE:
        compiler(
                'javascript',
                problem_path=hello_world_problem['path'],
                solution_source_path=Path(CE_solution_path)
            )
    assert 'solution.source:1' in str(CE.value)
