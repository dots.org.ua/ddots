# encoding: utf-8

from pathlib import Path
from unittest.mock import patch, MagicMock

from worker import utils


def test_download_file(tmpdir):
    from worker.extensions.api import DOTSApi

    class Response(object):
        def __init__(self, status_code, content, headers=None):
            self.status_code = status_code
            self.content = content
            self.headers = headers

        def iter_content(self, **kwargs):
            # pylint: disable=unused-argument
            return self.content

    local_file_path = tmpdir.join('1.txt')

    response_content = [b"File ", b"Content."]

    response = Response(status_code=200, content=response_content, headers={'ETag': '"etag"'})
    session_get = MagicMock(return_value=response)
    is_new = utils.download_file('http://q/1.txt', Path(local_file_path), session_get=session_get)

    assert is_new
    assert session_get.call_count == 1
    assert local_file_path.read_binary() == b''.join(response_content)
    assert local_file_path.new(ext='.txt.etag').read_binary() == b'etag'

    # Downloading an existing file should be skipped
    response = Response(status_code=304, content=response_content + [b"IGNORED"])
    session_get = MagicMock(return_value=response)
    is_new = utils.download_file('http://q/1.txt', Path(local_file_path), session_get=session_get)

    assert not is_new
    assert session_get.call_count == 1
    assert local_file_path.read_binary() == b''.join(response_content)
