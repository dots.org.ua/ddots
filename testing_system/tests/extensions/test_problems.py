# encoding: utf-8
import os
from pathlib import Path
import tarfile
from unittest.mock import patch

import pytest

from worker.extensions import problems


class App(object):
    class config(object):
        PROBLEMS_DB_ROOT = None
        PROBLEMS_DB_MAX_CACHED_PROBLEMS = 1000
        DOTS_PROBLEM_DOWNLOAD_TIMEOUT = 0.1


def test_download_problem_package(tmpdir):
    local_folder = tmpdir.mkdir('1001')
    local_folder.join('Problem.xml').write(
            '<?xml version="1.0" encoding="windows-1251"?><Problem></Problem>'
        )
    download_folder = tmpdir.mkdir('download')
    problem_archive_path = Path(download_folder.join('1001.tar.gz'))
    with tarfile.open(problem_archive_path, 'w:gz') as problem_package:
        problem_package.add(str(local_folder), arcname='1001')

    with patch('worker.extensions.problems.download_file', return_value=True):
        problems.download_problem_package(
                'http://127.0.0.1/1001.tar.gz',
                Path(download_folder.join('1001'))
            )

    assert download_folder.join('1001').check()
    assert download_folder.join('1001').join('Problem.xml').check()


@pytest.mark.asyncio
async def test_ProblemsManager(hello_world_problem):
    class TestApp(App):
        class config(App.config):
            PROBLEMS_DB_ROOT = Path(hello_world_problem['path'].parent)

    problems_manager = problems.ProblemsManager(app=TestApp())

    assert problems_manager.get_problem_path(
            hello_world_problem['problem_id']
        ) == hello_world_problem['path']

    with patch('worker.extensions.problems.download_file', return_value=False):
        await problems_manager.prepare_problem(hello_world_problem['problem_id'])
    assert hello_world_problem['path'].exists()
    assert (hello_world_problem['path'] / 'Problem.xml').exists()


def test_ProblemsManager_trim_problems_db_cache(hello_world_problem):
    class TestApp(App):
        class config(App.config):
            PROBLEMS_DB_ROOT = Path(hello_world_problem['path'].parent)
            PROBLEMS_DB_MAX_CACHED_PROBLEMS = 1

    problems_manager = problems.ProblemsManager(app=TestApp())

    another_older_problem_path = hello_world_problem['path'].parent / 'another_older_problem'
    another_older_problem_path.mkdir()
    another_older_problem_etag_path = another_older_problem_path.with_suffix('.tar.gz.etag')
    another_older_problem_etag_path.write_text("1")
    os.utime(another_older_problem_etag_path, times=(1, 1))
    another_problem_path = hello_world_problem['path'].parent / 'another_problem'
    another_problem_path.mkdir()
    another_problem_etag_path = another_problem_path.with_suffix('.tar.gz.etag')
    another_problem_etag_path.write_text("1")

    with patch('worker.extensions.problems.download_file', return_value=False):
        problems_manager.trim_problems_db_cache(exclude={hello_world_problem['path'].name})
    assert hello_world_problem['path'].exists()
    assert (hello_world_problem['path'] / 'Problem.xml').exists()

    assert another_problem_path.exists()
    another_problem_path.rmdir()
    assert another_problem_etag_path.exists()
    another_problem_etag_path.unlink()
    assert not another_older_problem_path.exists()
    assert another_older_problem_etag_path.exists()
    another_older_problem_etag_path.unlink()


def test_ProblemsDockerManagerMixin(tmpdir, hello_world_problem):
    class ProblemsDockerManagerMixin(problems.ProblemsDockerManagerMixin):
        _shared_volumes = {
                'data': {
                        'mount_point': Path(tmpdir),
                    },
            }

    problems_docker_manager_mixin = ProblemsDockerManagerMixin()
    problems_docker_manager_mixin.prepare_environment(hello_world_problem['path'])

    current_problem_root = tmpdir.join(ProblemsDockerManagerMixin.CURRENT_PROBLEM_DIRNAME)
    assert current_problem_root.join(ProblemsDockerManagerMixin.PROBLEM_XML_NAME).check()
    expected_problem_folder_files = set(os.listdir(hello_world_problem['path']))
    assert set(os.listdir(str(current_problem_root))) == expected_problem_folder_files
