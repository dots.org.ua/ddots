# encoding: utf-8
import functools
from pathlib import Path
import shutil

import pytest

from worker.exceptions import CompilationError
from worker.extensions import compilers


class App(object):
    class config(object):
        DOCKER_DAEMON_API_URL = 'unix:///var/run/docker.sock'
        CONTAINER_MEMORY_HARD_LIMIT = 256 * 1024 * 1024
        BASE_CONTAINER_NAME = 'compilers_testing'
        SHARED_VOLUMES = {
                'data': {
                        'volume_name': 'ddots-testing-system-testing-data',
                        'mount_point': Path('/data'),
                    },
                'sandbox': {
                        'volume_name': 'ddots-testing-system-testing-sandbox',
                        'mount_point': Path('/sandbox'),
                    },
            }
        DOTS_LANG_ID_TO_COMPILER = {}


@pytest.fixture(scope='session')
def compilers_manager():
    _compilers_manager = compilers.CompilersManager(app=App())
    _compilers_manager.init_containers_list(['bash'])
    yield _compilers_manager
    _compilers_manager.rm_container('bash')


def test_CompilersManager_start_OK(compilers_manager, tmpdir, hello_world_problem):
    OK_solution_path = tmpdir.join('OK_solution.sh')
    OK_solution_path.write('echo "Hello World!"')
    compilers_manager.start(
            'bash',
            problem_path=hello_world_problem['path'],
            solution_source_path=Path(OK_solution_path)
        )


def test_CompilersManager_start_CE(compilers_manager, tmpdir, hello_world_problem):
    CE_solution_path = tmpdir.join('CE_solution.sh')
    CE_solution_path.write('if')
    with pytest.raises(CompilationError):
        compilers_manager.start(
                'bash',
                problem_path=hello_world_problem['path'],
                solution_source_path=Path(CE_solution_path)
            )


def test_CompilersManager_start_with_patcher_OK(compilers_manager, tmpdir, hello_world_problem):
    tmp_problem = tmpdir.join('problem')
    shutil.copytree(hello_world_problem['path'], tmp_problem)
    problem_xml = tmp_problem.join('Problem.xml')
    problem_xml.write(problem_xml.read().replace(
            'CheckerExe="check"',
            'PatcherExe="patch"\nCheckerExe="check"'
        ))
    tmp_problem.join('patch').write(
            '#!/bin/sh\n'
            'sed "s/Helo/Hello/"'
        )
    OK_solution_path = tmpdir.join('OK_solution.sh')
    OK_solution_path.write('echo "Helo World!"')
    compilers_manager.start(
            'bash',
            problem_path=Path(tmp_problem),
            solution_source_path=Path(OK_solution_path)
        )
