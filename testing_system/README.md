Testing System
==============

This is a testing systems itself. It is written in Python and its main goals are to communicate
with Dots (download tests and solutions, report back results) and to manage Docker
containers (compilers and runners).
