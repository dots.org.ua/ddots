DDOST on Boot2Docker
====================

Boot2Docker is perfect if you deploy it on Virtual Machines.

It is tested on:

* Microsoft Hyper-V Server (we have a stable production)
* VMware
* Oracle VirtualBox


Step 1 (Setup Boot2Docker itself)
---------------------------------

1. Create a new VM target x64 arch (256MB RAM and 2GB storage is enough, but you may consider
using 256MB RAM per core and 10GB storage)
1. Boot from [Boot2Docker ISO](https://github.com/boot2docker/boot2docker/releases)
1. (VMware does this for you, so this step is for VirtualBox) Do the following in the console:

    ```
    root@boot2docker:~# docker run -it --privileged alpine sh
    / # apk add --no-cache e2fsprogs
    / # fdisk /dev/sda
    Command: o
    Command: n
    Select: p
    Partition: <enter>
    First sector: <enter>
    Last sector: <enter>
    Command: w
    / # mkfs.ext4 -L boot2docker-data /dev/sda1
    ...
    fs created label boot2docker-data on /dev/sda1
    ```
1. **Reboot**


Step 2 (optional)
-----------------

You will need to copy some commands into the terminal, but VirtualBox cannot do that, though VMware
have menu `Edit -> Paste` and you are good. Thus, we will set a password (it will last till the
next reboot) for `docker` user and ssh into the virtual machine from any where else to paste the
commands.

1. Change password:

    ```bash
    root@boot2docker:~# passwd docker
    ...
    ```
1. SSH from you laptop.


Step 3 (Prepare Boot2Docker environment)
----------------------------------------

Create a `start-env.sh` script, which must be used every time we need to
manipulate with DDOTS:

```bash
docker@boot2docker:~$ sudo tee /mnt/sda1/start-env.sh << 'EOF'
DOCKER_BIN="$(which docker)"
MNT_ROOT="/mnt/sda1"
ROOT_HOME="$MNT_ROOT/root"
WORKDIR="$MNT_ROOT/ddots-testing-system"
DOCKER_ENV_IMAGE="frolvlad/alpine-env"

# Fallback WORKDIR to MNT_ROOT if not exists
if [ ! -d "$WORKDIR" ]; then
    WORKDIR="$MNT_ROOT"
fi

# Precreate ddots in /opt
sudo mkdir -p /mnt/sda1/opt/ddots
if [ ! -e /opt/ddots ]; then
    sudo ln -s $MNT_ROOT/opt/ddots /opt/ddots
fi

# Make docker run interactive if we have stdin
if [ -t 0 ]; then
    # If we have TTY, then enable these options
    DOCKER_ARGS="--interactive --tty $DOCKER_ARGS"
fi

docker pull "$DOCKER_ENV_IMAGE"

docker run $DOCKER_ARGS --rm \
    --privileged \
    --pid=host \
    --ipc=host \
    --net=host \
    --volume "/tmp:/tmp" \
    --volume "/opt:/opt" \
    --volume "$ROOT_HOME/:/root/" \
    --volume "$DOCKER_BIN:$DOCKER_BIN" \
    --volume "/var/run/docker.sock:/var/run/docker.sock" \
    --volume "/sys/fs/cgroup:/sys/fs/cgroup" \
    --volume "$MNT_ROOT:$MNT_ROOT" \
    --workdir "$WORKDIR" \
    "$DOCKER_ENV_IMAGE" "$@"
EOF
```

Go inside of our environment (do it always when do something with DDOTS!):

```bash
docker@boot2docker:~$ sh /mnt/sda1/start-env.sh
```


Step 4 (Install DDOTS)
----------------------

Read more [here](INSTALL_DDOTS.md). (This step is the same for all deployment OSes)


Step 5 (Setup DDOTS autostart)
------------------------------

Make `docker` user's password persistent and add DDOTS to autostart:

```bash
docker@boot2docker:~$ sudo tee /var/lib/boot2docker/profile << 'EOF'
DOCKER_TLS=no
DOCKER_HOST=" "
EOF

docker@boot2docker:~$ sudo tee /var/lib/boot2docker/bootlocal.sh << 'EOF'
USER_BOOTLOCAL=/var/lib/boot2docker/bootlocal.user.sh
if [ -e "$USER_BOOTLOCAL" ]
then
    source "$USER_BOOTLOCAL"
fi

# Restore /etc/passwd and /etc/shadow after reboot
if [ -f /var/lib/boot2docker/etc/passwd ]; then
    rm /etc/passwd /etc/shadow
else
    mv /etc/passwd /etc/shadow /var/lib/boot2docker/etc/
fi
ln -s /var/lib/boot2docker/etc/passwd /var/lib/boot2docker/etc/shadow /etc

# Disable zram-based swap due to the issue when using zram,
# kernel-memory-limit, and memory-limit together with a heavy writing on a
# tmpfs partition.
swapoff /dev/zram0 || true

# Use lazytime with strictatime for proper problem packages cache invalidation
mount -o remount,lazytime,strictatime /mnt/sda1

# HOTFIX: boot2docker issue #1098: https://github.com/boot2docker/boot2docker/issues/1098
if [ ! -e /dev/mqueue ]
then
    mkdir /dev/mqueue
    mount -t mqueue none /dev/mqueue
fi

# DDOTS update, cleanup and start
sh -c '
    sleep 10 &&
    sh /mnt/sda1/start-env.sh sh -c "make update stop clean_dangling_docker_images start"
' &
EOF

docker@boot2docker:~$ sudo chmod +x /var/lib/boot2docker/bootlocal.sh
docker@boot2docker:~$ sudo /var/lib/boot2docker/bootlocal.sh
docker@boot2docker:~$ sudo passwd docker
```


Done
----

We are all set! Check the logs:

1. Activate environment:

    ```bash
    docker@boot2docker:~$ sh /mnt/sda1/start-env.sh
    ```
1. Check the logs:

    ```bash
    [affb1c747892 root:/mnt/sda1/ddots-testing-system]# make logs
    ```
