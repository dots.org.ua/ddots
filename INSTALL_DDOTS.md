Install DDOTS itself
====================

IMPORTANT: Please, prepare all OS requirements as described in
[README](README.md) (and relevant `INSTALL_ON_*` instructions) before
proceeding.


Download DDOTS
--------------

```bash
$ git clone --recurse-submodules https://gitlab.com/dots.org.ua/ddots.git
```


Initialize DDOTS
----------------

```bash
$ cd ddots
$ make install
```


Configure DDOTS
---------------

Copy `testing_system/local_settings.py.template` to `testing_system/local_settings.py`
and edit it (DOTS URL, DOTS username/password, time factor, and etc).
