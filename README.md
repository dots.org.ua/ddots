Testing System for DOTS
=======================

NOTE: Short Russian version is avaiable in README_ru.md.

NOTE: Короткая русская версия доступна в README_ru.md.


Overview
--------

DDOTS (Dockerized Distributed Olympiad Testing System) is a testing system for programming contests
written in Python and Rust and is intensively using Docker containers and Cgroup kernel subsystems
to run compilers and to test users' solutions.

This system is very flexible by design! Each compiler is a separate Docker image, so you can easily
add, update, and remove compilers. The same story is for 'runners', which are images, that aim to
run a solution (either a binary file from compilers, or byte code like JVM, or even a script like
Python/Bash).

One instance of DDOTS works with one solution at a time! Thus, it is recommended to simply run N
containers of DDOTS to load N cores of a given machine.

Check out wiki pages for more details.


Installation
------------

You can decide on which platform you want to deploy DDOTS:

* Boot2Docker is perfect if you deploy it on Virtual Machines (tested extensively)
  ([instructions](INSTALL_ON_BOOT2DOCKER.md))
* Ubuntu is great for bare metal setup (known issues require Linux Kernel 4.0+ and AUFS support)
  ([instructions](INSTALL_ON_UBUNTU.md))


Update DDOTS
------------

NOTE: If you have setup DDOTS autostart, you don't need to do this manualy as there is an
autoupdate script on startup, so just reboot your boot2docker or ddots-service (on Ubuntu).

In general case, once system is deployed you can update it with the following command:

```bash
$ make update
```

Restart DDOTS after update.


Usage
-----

NOTE: If you are using boot2docker setup, don't forget to activate the environment!

### Testing System

Start:

```bash
$ make start
```

Note: You can use `DDOTS_CPU_COUNT` env variable to control the number of ddots
instances. By default, it will create an instance for each core. See other
supported environment variables in the `Makefile`, `config.py`, and
`local_config.py`. Here is an example of passing environment variables:

```bash
$ env DDOTS_CPU_COUNT=2 make start
```

Watch logs:

```bash
$ make logs
```

Stop:

```bash
$ make stop
```

Restart:

```bash
$ make restart
```

### Checkers

Compile a checker (`check.cpp` to `check`):

```bash
$ docker run -it --rm --volume `pwd`:/mnt --workdir /mnt frolvlad/alpine-gxx sh -c 'c++ --static -s -O3 -I. check.cpp -o check ; chown 1000:1000 check'
```

Batch recompile checkers in KHCUP contests (`0[0-9][0-9]/*cmp.cpp` to `check`):

```bash
$ cd problems
$ find . \
    -name '0[0-9][0-9]' \
    -type d \
    -print \
    -exec \
        docker run \
            -it \
            --rm \
            --volume "$(pwd)/{}:/mnt" \
            --workdir /mnt \
            frolvlad/alpine-gcc \
                sh -c '\
                    c++ --static -s -O3 -I. *cmp.cpp -o check ; \
                    chown 1000:1000 check ; \
                ' \
    \;
```

### Manual Testing

NOTE: If you are using boot2docker setup, don't forget to activate the environment!

Test one given solution:

```bash
$ #export PROGRAMMING_LANGUAGE=(gcc|gcc-c11|gpp|gpp-cpp11|fpc|fpc-delphi|openjdk7|oraclejdk7|mono|go|python2|python3|...)
$ export PROGRAMMING_LANGUAGE=gpp

$ #export PROBLEM_ROOT=<path to the tests root>
$ export PROBLEM_ROOT=/tmp/tests

$ #export SOLUTION=<path to the solution source>
$ export SOLUTION=/tmp/qq.cpp

$ #export TESTING_MODE=(full|first-fail|one)
$ export TESTING_MODE=full

$ make test_solution
```

OR you can always do it in one line:

```bash
$ PROGRAMMING_LANGUAGE=gpp PROBLEM_ROOT=/tmp/tests SOLUTION=/tmp/qq.cpp TESTING_MODE=full make test_solution
```


Useful Files and Directories
----------------------------

* `/opt/ddots/problems_db` inside the running ddots-testing-system-* containers
contains tarballs and unpacked tests; the Testing System downloads necessary
tests on demand and cleans them on restart;
* `./testing_system/local_config.py` - this file is what you need to use to configure
the Testing System;
* `./examples/problems_db/` - contains a nice and complete example of a usual DOTS problem


Performance Notes
-----------------

See PERFORMANCE.md file.


Security Notes
--------------

See SECURITY.md file.
