FROM frolvlad/alpine-python3

# Makefile template
#ENV DOCKER_GID=X
# boot2docker
#ENV DOCKER_GID=100
# Ubuntu
#ENV DOCKER_GID=999
# Ubuntu Core
#ENV DOCKER_GID=123

RUN if [ -z "$DOCKER_GID" ] ; then echo $'\n\n\n\033[1;31m!!! Uncomment one ENV DOCKER_GID in ./Dockerfile !!!\033[0m\n\n\n' ; exit 1 ; fi && \
    echo $'root:x:0:root\ndocker:x:'$DOCKER_GID$':operator\nnobody:x:65534:nobody' > /etc/group && \
    sed -i 's|operator:/root|operator:/tmp|' /etc/passwd

WORKDIR /opt/ddots/testing_system
COPY ./testing_system/requirements.txt ./requirements.txt
RUN apk add --no-cache ca-certificates && \
    pip3 install -r ./requirements.txt

# We exploit Docker bug-feature to ensure the right permissions on the mount
# points: https://github.com/docker/docker/issues/28041
RUN mkdir /data && \
    chown operator:root /data && \
    chmod 755 /data && \
    mkdir /sandbox && \
    chown operator:root /sandbox && \
    chmod 777 /sandbox

USER operator
COPY ./testing_system/ ./

CMD ["/bin/sh"]
