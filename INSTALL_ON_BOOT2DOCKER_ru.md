DDOST on Boot2Docker
====================

Boot2Docker is perfect if you deploy it on Virtual Machines.

DDOTS в режиме Boot2Docker тестировался на:

* Microsoft Hyper-V Server (есть стабильный сервер, где эта конфигурация уже давно отлично
  работает)
* VMware
* Oracle VirtualBox


Шаг 1 (Установка самого Boot2Docker)
------------------------------------

1. Создаём новую виртуальную машину для x64 архитектуры (проверено на 256МБ ОЗУ, компиляторы
занимают 3.3ГБ, тесты сейчас занимают 1.6ГБ, так что размер раздела может быть даже 7ГБ (для того
чтобы хватало места при обновлении образов компиляторов), хотя, просто для запаса лучше выделить
10ГБ и никогда потом не думать о проблемах с местом)
1. Загружаемся с [Boot2Docker ISO](https://github.com/boot2docker/boot2docker/releases)
1. (VMware делает это автоматически, VirtualBox - делаем сами) Форматируем раздел и помечаем его меткой `boot2docker-data`:

    ```
    root@boot2docker:~# docker run -it --privileged alpine sh
    / # apk add --no-cache e2fsprogs
    / # fdisk /dev/sda
    Command: o
    Command: n
    Select: p
    Partition: <enter>
    First sector: <enter>
    Last sector: <enter>
    Command: w
    / # mkfs.ext4 -L boot2docker-data /dev/sda1
    ...
    fs created label boot2docker-data on /dev/sda1
    ```
1. **Перегружаем систему**


Шаг 2 (опционально)
-------------------

Вам нужно будет скопировать несколько команд в терминал на следующем шаге, но VirtualBox
не умеет этого, в VMware есть меню `Edit -> Paste`. Таким образом, для решения этой задачи
предлагается использовать ssh, а для этого нужно установить пароль для пользователя `docker`
(этот пароль сбросится после перезагрузки).

1. Меняем пароль:

    ```bash
    root@boot2docker:~# passwd docker
    ...
    ```
1. Подключаемся по SSH


#### Шаг 3 (Подготовка окружения Boot2Docker)

Создаём скрипт `start-env.sh`, который мы будем использовать всегда для управления DDOTS
(в boot2docker не хватает bash, make, perl (для git)):

```bash
docker@boot2docker:~$ sudo tee /mnt/sda1/start-env.sh << 'EOF'
DOCKER_BIN="$(which docker)"
MNT_ROOT="/mnt/sda1"
ROOT_HOME="$MNT_ROOT/root"
WORKDIR="$MNT_ROOT/ddots-testing-system"
DOCKER_ENV_IMAGE="frolvlad/alpine-env"

# Fallback WORKDIR to MNT_ROOT if not exists
if [ ! -d "$WORKDIR" ]; then
    WORKDIR="$MNT_ROOT"
fi

# Precreate ddots in /opt
sudo mkdir -p /mnt/sda1/opt/ddots
if [ ! -e /opt/ddots ]; then
    sudo ln -s $MNT_ROOT/opt/ddots /opt/ddots
fi

# Make docker run interactive if we have stdin
if [ -t 0 ]; then
    # If we have TTY, then enable these options
    DOCKER_ARGS="--interactive --tty $DOCKER_ARGS"
fi

docker pull "$DOCKER_ENV_IMAGE"

docker run $DOCKER_ARGS --rm \
    --privileged \
    --pid=host \
    --ipc=host \
    --net=host \
    --volume "/tmp:/tmp" \
    --volume "/opt:/opt" \
    --volume "$ROOT_HOME/:/root/" \
    --volume "$DOCKER_BIN:$DOCKER_BIN" \
    --volume "/var/run/docker.sock:/var/run/docker.sock" \
    --volume "/sys/fs/cgroup:/sys/fs/cgroup" \
    --volume "$MNT_ROOT:$MNT_ROOT" \
    --workdir "$WORKDIR" \
    "$DOCKER_ENV_IMAGE" "$@"
EOF
```

"Активируем" окружение (в этом окружении есть make, git, vim и немного цветов - всегда активируйте
окружение при работе с командами DDots, так как они записаны в Makefile):

```bash
docker@boot2docker:~$ sh /mnt/sda1/start-env.sh
```


Шаг 4 (Установка DDOTS)
----------------------

Этот шаг идентичен для всех ОС, поэтому вынесен в отдельный файл [инструкции](INSTALL_DDOTS.md).


Шаг 5 (Настройка автозапуска DDOTS)
------------------------------

Делаем пароль пользователя `docker` постоянным и добавляем DDots в автозагрузку:

```bash
docker@boot2docker:~$ sudo tee /var/lib/boot2docker/profile << 'EOF'
DOCKER_TLS=no
DOCKER_HOST=" "
EOF

docker@boot2docker:~$ sudo tee /var/lib/boot2docker/bootlocal.sh << 'EOF'
USER_BOOTLOCAL=/var/lib/boot2docker/bootlocal.user.sh
if [ -e "$USER_BOOTLOCAL" ]
then
    source "$USER_BOOTLOCAL"
fi

# Restore /etc/passwd and /etc/shadow after reboot
if [ -f /var/lib/boot2docker/etc/passwd ]; then
    rm /etc/passwd /etc/shadow
else
    mv /etc/passwd /etc/shadow /var/lib/boot2docker/etc/
fi
ln -s /var/lib/boot2docker/etc/passwd /var/lib/boot2docker/etc/shadow /etc

# Disable zram-based swap due to the issue when using zram,
# kernel-memory-limit, and memory-limit together with a heavy writing on a
# tmpfs partition.
swapoff /dev/zram0 || true

# Use lazytime with strictatime for proper problem packages cache invalidation
mount -o remount,lazytime,strictatime /mnt/sda1

# HOTFIX: boot2docker issue #1098: https://github.com/boot2docker/boot2docker/issues/1098
if [ ! -e /dev/mqueue ]
then
    mkdir /dev/mqueue
    mount -t mqueue none /dev/mqueue
fi

# DDOTS update, cleanup and start
sh -c '
    sleep 10 &&
    sh /mnt/sda1/start-env.sh sh -c "make update stop clean_dangling_docker_images start"
' &
EOF

docker@boot2docker:~$ sudo chmod +x /var/lib/boot2docker/bootlocal.sh
docker@boot2docker:~$ sudo /var/lib/boot2docker/bootlocal.sh
docker@boot2docker:~$ sudo passwd docker
```


Готово!
-------

Проверяем, что всё работает:

1. Активируем окружение (как уже было сказано - это лучше делать всегда)

    ```bash
    docker@boot2docker:~$ sh /mnt/sda1/start-env.sh
    ```
1. Смотрим логи:

    ```bash
    [affb1c747892 root:/mnt/sda1/ddots-testing-system]# make logs
